export default {
    port: 8085,
    currency: '€',
    maxPackageWeight: 10000, // In grams
    maxGiftPerPackage: 15,
    maxGiftWeight: 10000, // In grams
    maxGiftCost: 100
}