import { Request, Response, NextFunction } from "express";

const validateRequests =
  (queryParams: string[]) =>
    (request: Request, response: Response, next: NextFunction) => {
      for (let queryParam of queryParams) {
        switch (queryParam) {
          case 'filePath':
            if (!request.query[queryParam]) {
              const errorObj = JSON.stringify({ 
                status: 'fail',
                error: `${queryParam} is missing in the URL`
            });
              return response.status(400).send(errorObj);
            }
            break;
        }
      }
      next();
    };

export default validateRequests;