import { Request, Response } from "express";
import { getPackage } from "../service/packer.service";

const packageHandler = async (request: Request, response: Response) => {
    response.setHeader('Content-Type', 'application/json');
    try {
        const filePath = request.query.filePath as string;
        const gifts = await getPackage(filePath).then(
            (data: any) => {
                const successObj = JSON.stringify({ 
                    status: 'success',
                    data: data
                });
                return response.status(200).send(successObj);
            }
        ).catch((error: any) => {
            const errorObj = JSON.stringify({ 
                status: 'fail',
                error: error.message
            });
            return response.status(500).send(errorObj);
        });
    } catch (error: any) {
        const errorObj = JSON.stringify({ 
            status: 'fail',
            error: error.message
        });
        return response.status(500).send(errorObj);
    }
}

export default packageHandler;
