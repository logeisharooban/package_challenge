export interface Gift {
    index: number;
    weight: number;
    cost: number;
}