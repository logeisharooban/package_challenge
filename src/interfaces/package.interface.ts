import { Gift } from "./gift.interface";

export interface PackageLineItem {
    maxWeight: number;
    giftsAsString: string;
}

export interface Package {
    maxWeight: number;
    availableGifts: Gift[];
}