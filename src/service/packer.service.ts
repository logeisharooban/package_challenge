import { Gift } from "../interfaces/gift.interface";
import { readFile, readFileSync } from "fs";
import config from "config";
import { Package, PackageLineItem } from "../interfaces/package.interface";

/**
 * Read file and get maximum possible gifts
 * @param filePath
 * @returns Max possible gifts by reading the sample input
 */
const getPackage = async (filePath: string) => {
    // const filePath = "./resources/example_input";
    return readTestFile(filePath)
        .then(async (data: any) => {
            let result = [];
            for (let packageLineItemStr of data) {
                try {
                    const packageItem = await getPackageItem(String(packageLineItemStr));
                    const possibleMaxGifts = await getPossibleGifts(packageItem);
                    const maxPossibleMaxGifts = await getMaxPossibleGifts(packageItem, possibleMaxGifts);
                    result.push(maxPossibleMaxGifts);
                } catch (error: any) {
                    return error.message
                }
            }
            return result;
        })
        .catch((error) => {
            console.log(error.message);
            return error.message;
        });
};

/**
 * Asyncronusly read the file and creates an array of each lines
 * @param filePath
 * @returns Read file lines in array
 */
const readTestFile = (filePath: string): Promise<any> => {
    return new Promise((resolve, reject) => {
        return readFile(filePath, "utf8", (error, result) => {
            if (error) {
                reject(error);
            } else {
                resolve(result.toString().split("\n"));
            }
        });
    });
};

/**
 * Get available package gift items as an array with each packs
 * @param packageLineItemStr
 * @returns Package line item object 
 */
const getPackageItem = async (packageLineItemStr: string) => {
    // console.log(packageLineItemStr);
    try {
        const packageLineItemArr = packageLineItemStr.split(":");
        const packageLineItem: PackageLineItem = {
            maxWeight: parseInt(packageLineItemArr[0]),
            giftsAsString: packageLineItemArr[1],
        };

        // Following code contain the logic for
        // 1 - remove spaces fron everywhere
        // 2 - get the string from parentheses
        // 3 - remove empty arrays
        // 4 - create gift objects
        const availableGifts: Gift[] = packageLineItem.giftsAsString
            .replace(/[€\s]+/g, "")
            .split(/\(([^)]*)\)/)
            .filter(String)
            .map((gift) => {
                const giftArr = gift.split(",").map(Number);
                return {
                    index: Number(giftArr[0]),
                    weight: Math.ceil(Number(giftArr[1] * 100)), // In grams
                    cost: Number(giftArr[2]),
                };
            });

        const packageItem: Package = {
            maxWeight: packageLineItem.maxWeight * 100, // In grams
            availableGifts: availableGifts,
        };
        return packageItem;
    } catch (error: any) {
        return error.message;
    }
};

/**
 * Get maximum possible gifts for the given package capacity
 * @param packageItem Package
 * @returns Max possible gifts
 */
const getPossibleGifts = async (packageItem: any) => {
    let possibleGifts = [];
    // Constrain 1 = giftWeight/s cannot > than packageWeight
    // Constrain 2 = Max weight that a package can take is ≤ 100
    // Constrain 4 = Max weight and cost of an item is ≤ 100
    for (let availableGift of packageItem.availableGifts) {
        if (
            availableGift.weight <= packageItem.maxWeight &&
            availableGift.weight <= config.get<number>("maxGiftWeight") &&
            packageItem.maxWeight <= config.get<number>("maxPackageWeight") &&
            availableGift.cost <= config.get<number>("maxGiftCost")
        ) {
            possibleGifts.push(availableGift as Gift);
        }
    }
    return possibleGifts;
};

/**
 * Used algoriths for Knapsack problem
 * @param packageItem
 * @param possibleGifts
 * @returns max cost filled weight gifts
 */
const getMaxPossibleGifts = async (packageItem: Package, possibleGifts: Gift[]) => {

    // Decending order gifts by cost
    possibleGifts.sort((giftCurrent: Gift, giftNext: Gift) => {
        return giftCurrent.weight - giftNext.weight;
    });

    let weights = possibleGifts.map((gift) => gift.weight);
    let costs = possibleGifts.map((gift) => gift.cost);
    let capacity = packageItem.maxWeight;

    // Simple validation to check all the params are ok
    const n = costs.length;
    if (capacity <= 0 || n == 0 || weights.length != n) {
        return 0;
    }

    // Create 2D chart(array) with 0 values
    const chart = Array(costs.length)
        .fill(0)
        .map(() => {
            return Array(capacity + 1).fill(0)
        });

    // Populate the capacity=0 columns with '0' capacity we have '0' cost
    for (let i = 0; i < n; i++) {
        chart[i][0] = 0;
    }

    // If there is one weight, take it if it is not more than the capacity
    for (let c = 0; c <= capacity; c++) {
        if (weights[0] <= c) {
            chart[0][c] = costs[0];
        }
    }

    // Process all charts for all the capacities
    for (let i = 1; i < n; i++) {
        for (let c = 1; c <= capacity; c++) {
            let cost1 = 0,
                cost2 = 0;

            // include the item, if it is not more than the capacity
            if (weights[i] <= c) {
                cost1 = costs[i] + chart[i - 1][c - weights[i]];
            }

            // exclude the item
            cost2 = chart[i - 1][c];

            // take maximum
            chart[i][c] = Math.max(cost1, cost2);
        }
    }

    // Finding the solution sfrom KnapSack chart
    let totalWeight = chart[weights.length - 1][capacity];
    let selectedGifts = [];
    let remainingCapacity = capacity;

    for (let i = weights.length - 1; i > 0; i--) {
        if (totalWeight != chart[i - 1][remainingCapacity]) {
            remainingCapacity -= weights[i];
            totalWeight -= costs[i];
            selectedGifts.push(possibleGifts[i].index);
        }
    }

    // If the total weight not fill by max cost take the first one
    if (totalWeight != 0) {
        selectedGifts.push(possibleGifts[0].index);
    }

    return selectedGifts;
};


// export default {getPackage, readTestFile, getPackageItem, getPossibleGifts};
export { getPackage, readTestFile, getPackageItem, getPossibleGifts, getMaxPossibleGifts };
