import assert from "assert";
import { Package, PackageLineItem } from "../interfaces/package.interface";
import {getPackage, readTestFile, getPackageItem, getPossibleGifts, getMaxPossibleGifts} from "./packer.service";

const filePath = './resources/example_input';
const packageStr = '81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)';
const packageItemMock = {
  maxWeight: 8100,
  availableGifts: [
    { index: 1, weight: 5338, cost: 45 },
    { index: 2, weight: 8862, cost: 98 },
    { index: 3, weight: 7848, cost: 3 },
    { index: 4, weight: 7230, cost: 76 },
    { index: 5, weight: 3018, cost: 9 },
    { index: 6, weight: 4634, cost: 48 }
  ]
};

const possibleGiftsMock = [
  { index: 1, weight: 5338, cost: 45 },
  { index: 3, weight: 7848, cost: 3 },
  { index: 4, weight: 7230, cost: 76 },
  { index: 5, weight: 3018, cost: 9 },
  { index: 6, weight: 4634, cost: 48 }
];

describe('getPackage', () => {
  it('should return results as array if the correct file path given', () => {
    return getPackage(filePath)
      .then(testFileArray => {
        expect(Array.isArray(testFileArray)).toBeTruthy();
      })
  });

  it('should reject if wrong or empty file path is given', () => {
    return getPackage('')
      .catch (e => expect(e).toBeTruthy());
  });
})

describe('readTestFile', () => {
    it('should reject if wrong or empty file path is given', () => {
      return readTestFile('')
        .catch (e => expect(e).toBeTruthy());
    });

    it('should return as an array', () => {
      return readTestFile(filePath)
        .then(testFileArray => {
          expect(Array.isArray(testFileArray)).toBeTruthy();
        })
    });

    it('should return expected row count', () => {
      return readTestFile(filePath)
        .then(testFileArray => {
          expect(testFileArray).toHaveLength(4);
        })
    });
  })

  describe('getPackageItem', () => {
    /* it('should have the return type as Package', () => {
      const packageItem = getPackageItem(packageStr);
      console.log(typeof packageItem);
      expect(packageItem).toBeInstanceOf('Package');
    }); */

    it('should have availableGift and maxWeight property', async () => {
      const packageItem = await getPackageItem(packageStr);
      /* console.log('===============');
      console.log(packageItem);
      console.log('==============='); */
      expect(packageItem.hasOwnProperty('availableGifts')).toBeTruthy();
      expect(packageItem.hasOwnProperty('maxWeight')).toBeTruthy();
    });

    it('should have the availableGift array length 6', async () => {
      const packageItem = await getPackageItem(packageStr);
      expect(packageItem.availableGifts.length).toEqual(6);
    });

    it('should have the maxweight as 8100', async () => {
      const packageItem = await getPackageItem(packageStr);
      expect(packageItem.maxWeight).toEqual(8100);
    });
  })

  describe('getMaxPossibleGifts', () => {
    it('should return correct solution for knapsack problem', async () => {
      const maxPossibleGifts: any = await getMaxPossibleGifts(packageItemMock as any, possibleGiftsMock);
      // console.log('======>', maxPossibleGifts);
      expect(maxPossibleGifts[0]).toEqual(4);
    });
  })

  describe('getPossibleGifts', () => {
    it('should return the expected results as 4 and should have correct properties', async () => {
      const possibleGifts = await getPossibleGifts(packageItemMock as any);
      console.log(possibleGifts);
      expect(possibleGifts.length).toEqual(5);
      expect(possibleGifts[0].hasOwnProperty('index')).toBeTruthy();
      expect(possibleGifts[0].hasOwnProperty('weight')).toBeTruthy();
      expect(possibleGifts[0].hasOwnProperty('cost')).toBeTruthy();
    });

    it('should reject if max pack weight constrains are not met', async () => {
      const newPack = packageItemMock;
      newPack.maxWeight = 50000;
      const possibleGifts = await getPossibleGifts(packageItemMock as any);
      expect(possibleGifts.length).toEqual(0);
    });

    it('should reject if max gift weight constrains are not met', async () => {
      packageItemMock.availableGifts[5].weight = 50000;
      const possibleGifts = await getPossibleGifts(packageItemMock as any);
      expect(possibleGifts.length).toEqual(0);
    });

  })
