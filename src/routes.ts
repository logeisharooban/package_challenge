import { Express, Request, Response } from "express";
import packageHandler from "./controller/package.controller";
import validateRequests from "./middleware/validate";

const routes = (app: Express) => {
    app.get('/package', validateRequests(['filePath']), packageHandler);
}

export default routes;