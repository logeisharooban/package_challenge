## Setup

Use git for clone the project or extract the zip file. NodeJS must be installed in the machine.

```bash
git clone https://bitbucket.org/logeisharooban/package_challenge.git

npm install

npm start
```

## API

Use CURL command or use browser to test the http request. Middleware added with some simple validations.

```bash
curl http://localhost:8085/package?filePath=.%2Fresources%2Fexample_input
```

## Test

Unit test added using Jest and with some basic test cases. Main solutions are implemented in **packer.service.ts** and unit test added in **packer.service.test.ts**

```bash
npm test
```